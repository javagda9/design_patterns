package com.sda.dp.builder.przyklad;

public class Main {
    public static void main(String[] args) {
//        Hero.Builder builder = new Hero.Builder("Marian");
//        Hero stworzony = builder.setMana(50).setHp(30).setStamina(50).create();
        Hero stworzony = new Hero.Builder().setMana(50).setHp(30).setStamina(50).createHero();

        Enemy enemy = new Enemy.Builder().setName("Arek").createEnemy();
    }
}
