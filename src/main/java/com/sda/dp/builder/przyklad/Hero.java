package com.sda.dp.builder.przyklad;

public class Hero {
    private int hp, mana, stamina, attackPoints, defensePoints, height, speed;
    private String name, surname, secondName, mothersName, fathersName, grandmothersName;

    public Hero(int hp, int mana, int stamina, int attackPoints, int defensePoints, int height, int speed, String name, String surname, String secondName, String mothersName, String fathersName, String grandmothersName) {
        this.hp = hp;
        this.mana = mana;
        this.stamina = stamina;
        this.attackPoints = attackPoints;
        this.defensePoints = defensePoints;
        this.height = height;
        this.speed = speed;
        this.name = name;
        this.surname = surname;
        this.secondName = secondName;
        this.mothersName = mothersName;
        this.fathersName = fathersName;
        this.grandmothersName = grandmothersName;
    }

    public static class Builder {

        private int hp = 100;
        private int mana;
        private int stamina;
        private int attackPoints;
        private int defensePoints;
        private int height;
        private int speed;
        private String name;
        private String surname;
        private String secondName;
        private String mothersName;
        private String fathersName;
        private String grandmothersName;

        public Builder() {
            this.hp = 100;
        }

        public Builder setHp(int hp) {
            this.hp = hp;
            return this;
        }

        public Builder setMana(int mana) {
            this.mana = mana;
            return this;
        }

        public Builder setStamina(int stamina) {
            this.stamina = stamina;
            return this;
        }

        public Builder setAttackPoints(int attackPoints) {
            this.attackPoints = attackPoints;
            return this;
        }

        public Builder setDefensePoints(int defensePoints) {
            this.defensePoints = defensePoints;
            return this;
        }

        public Builder setHeight(int height) {
            this.height = height;
            return this;
        }

        public Builder setSpeed(int speed) {
            this.speed = speed;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setSurname(String surname) {
            this.surname = surname;
            return this;
        }

        public Builder setSecondName(String secondName) {
            this.secondName = secondName;
            return this;
        }

        public Builder setMothersName(String mothersName) {
            this.mothersName = mothersName;
            return this;
        }

        public Builder setFathersName(String fathersName) {
            this.fathersName = fathersName;
            return this;
        }

        public Builder setGrandmothersName(String grandmothersName) {
            this.grandmothersName = grandmothersName;
            return this;
        }

        public Hero createHero() {
            return new Hero(hp, mana, stamina, attackPoints, defensePoints, height, speed, name, surname, secondName, mothersName, fathersName, grandmothersName);
        }
    }
}
