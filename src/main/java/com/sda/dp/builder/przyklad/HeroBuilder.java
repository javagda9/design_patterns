package com.sda.dp.builder.przyklad;

public class HeroBuilder {
    private int hp;
    private int mana;
    private int stamina;
    private int attackPoints;
    private int defensePoints;
    private int height;
    private int speed;
    private String name;
    private String surname;
    private String secondName;
    private String mothersName;
    private String fathersName;
    private String grandmothersName;

    public HeroBuilder setHp(int hp) {
        this.hp = hp;
        return this;
    }

    public HeroBuilder setMana(int mana) {
        this.mana = mana;
        return this;
    }

    public HeroBuilder setStamina(int stamina) {
        this.stamina = stamina;
        return this;
    }

    public HeroBuilder setAttackPoints(int attackPoints) {
        this.attackPoints = attackPoints;
        return this;
    }

    public HeroBuilder setDefensePoints(int defensePoints) {
        this.defensePoints = defensePoints;
        return this;
    }

    public HeroBuilder setHeight(int height) {
        this.height = height;
        return this;
    }

    public HeroBuilder setSpeed(int speed) {
        this.speed = speed;
        return this;
    }

    public HeroBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public HeroBuilder setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public HeroBuilder setSecondName(String secondName) {
        this.secondName = secondName;
        return this;
    }

    public HeroBuilder setMothersName(String mothersName) {
        this.mothersName = mothersName;
        return this;
    }

    public HeroBuilder setFathersName(String fathersName) {
        this.fathersName = fathersName;
        return this;
    }

    public HeroBuilder setGrandmothersName(String grandmothersName) {
        this.grandmothersName = grandmothersName;
        return this;
    }

    public Hero createHero() {
        return new Hero(hp, mana, stamina, attackPoints, defensePoints, height, speed, name, surname, secondName, mothersName, fathersName, grandmothersName);
    }
}