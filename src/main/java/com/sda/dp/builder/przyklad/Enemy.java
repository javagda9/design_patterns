package com.sda.dp.builder.przyklad;

public class Enemy {
    private String name;
    private int power;

    public Enemy(String name, int power) {
        this.name = name;
        this.power = power;
    }

    public static class Builder{

        private String name;
        private int power;

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setPower(int power) {
            this.power = power;
            return this;
        }

        public Enemy createEnemy() {
            return new Enemy(name, power);
        }
    }
}
