package com.sda.dp.singleton.przyklad;

public class Poczta {
    private Automat automat = new Automat();
    private Rejestracja rejestracja = new Rejestracja();
    private AplikacjaInternetowa serwer = new AplikacjaInternetowa();

    public Ticket wygenerujAutomat() {
        return automat.wygeneruj();
    }

    public Ticket wygenerujSerwer() {
        return serwer.wygeneruj();
    }

    public Ticket wygenerujRejestracja() {
        return rejestracja.wygeneruj();
    }
}
