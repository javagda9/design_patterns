package com.sda.dp.singleton.przyklad;

public class AplikacjaInternetowa {

    public Ticket wygeneruj(){
        return new Ticket("AplikacjaInternetowa", TicketGenerator.INSTANCE.pobierzNumerek());
    }
}
