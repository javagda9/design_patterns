package com.sda.dp.singleton.przyklad;

public class Automat {
    public Ticket wygeneruj(){
        return new Ticket("Automat", TicketGenerator.INSTANCE.pobierzNumerek());
    }
}
