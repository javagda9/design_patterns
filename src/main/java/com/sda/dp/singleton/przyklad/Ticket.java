package com.sda.dp.singleton.przyklad;

public class Ticket {
    private String skad;
    private int numerek;

    public Ticket(String skad, int numerek) {
        this.skad = skad;
        this.numerek = numerek;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "skad='" + skad + '\'' +
                ", numerek=" + numerek +
                '}';
    }
}
