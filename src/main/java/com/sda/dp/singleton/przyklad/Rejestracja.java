package com.sda.dp.singleton.przyklad;

public class Rejestracja {

    public Ticket wygeneruj(){
        return new Ticket("Rejestracja", TicketGenerator.INSTANCE.pobierzNumerek());
    }
}
