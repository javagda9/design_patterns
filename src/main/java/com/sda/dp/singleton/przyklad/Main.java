package com.sda.dp.singleton.przyklad;

import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args) {

//        ExecutorService service = Executors.newFixedThreadPool(5);
//
//        for (int i = 0; i < 50; i++) {
//            service.submit(new Runnable() {
//                public void run() {
//                    TicketGenerator.getInstance().sleep();
//                }
//            });
//        }
        Poczta poczta = new Poczta();

        Scanner scanner = new Scanner(System.in);
        boolean praca = true;
        while (praca) {
            String linia = scanner.nextLine();
            if (linia.equalsIgnoreCase("1")) {
                System.out.println(poczta.wygenerujAutomat());
            } else if (linia.equalsIgnoreCase("2")) {
                System.out.println(poczta.wygenerujRejestracja());
            } else if (linia.equalsIgnoreCase("3")) {
                System.out.println(poczta.wygenerujSerwer());
            }
        }
    }
}
