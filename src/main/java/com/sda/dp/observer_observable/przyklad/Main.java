package com.sda.dp.observer_observable.przyklad;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        StacjaTelewizyjnaWiadomosci stacjaTelewizyjnaWiadomosci = new StacjaTelewizyjnaWiadomosci();
        stacjaTelewizyjnaWiadomosci.dodajOgladajacego(new Ogladajacy(5, "Marian"));
        stacjaTelewizyjnaWiadomosci.dodajOgladajacego(new Ogladajacy(3, "Arek"));
        stacjaTelewizyjnaWiadomosci.dodajOgladajacego(new Ogladajacy(6, "Darek"));
        stacjaTelewizyjnaWiadomosci.dodajOgladajacego(new Ogladajacy(7, "Marek"));
        stacjaTelewizyjnaWiadomosci.dodajOgladajacego(new Ogladajacy(8, "Franek"));
        stacjaTelewizyjnaWiadomosci.dodajOgladajacego(new Ogladajacy(9, "Julia"));

        stacjaTelewizyjnaWiadomosci.powiadom(10, "Trump prezydentem");


//        List<Person> list = new ArrayList<>();
//
//        list.add(new Person("a"));
//        list.add(new Person("b"));
//        list.add(new Person("c"));
//        list.add(new Person("d"));
//        list.add(new Person("e"));
//        list.add(new Person("f"));
//        list.add(new Person("g"));
//        list.add(new Person("h"));
//        list.add(new Person("i"));
//        list.add(new Person("f"));
//        list.add(new Person("k"));
//
//        Iterator<Person> personIterator = list.iterator();
//        while (personIterator.hasNext()){
//            Person p = personIterator.next();
//            if (p.getName().equalsIgnoreCase("f")) {
//                personIterator.remove();
//            }
//        }

//        for (Person p : list) {
//            if (p.getName().equalsIgnoreCase("f")) {
//                list.remove(p);
//            }
//        }

    }
}
