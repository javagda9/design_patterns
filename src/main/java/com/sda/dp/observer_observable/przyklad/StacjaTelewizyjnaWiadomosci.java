package com.sda.dp.observer_observable.przyklad;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class StacjaTelewizyjnaWiadomosci extends Observable {

    public void dodajOgladajacego(Ogladajacy ogladajacy) {
        addObserver(ogladajacy);
    }

    public void powiadom(int poziom, String oWiadomosci) {
        setChanged();
        notifyObservers(new Wiadomosc(poziom, oWiadomosci));
    }
}
