package com.sda.dp.observer_observable.przyklad;

import java.util.Observable;
import java.util.Observer;

public class Ogladajacy implements Observer {
    private int poziomPaniki;
    private String name;

    public Ogladajacy(int poziomPaniki, String name) {
        this.poziomPaniki = poziomPaniki;
        this.name = name;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Wiadomosc) {
            Wiadomosc wiadomosc = (Wiadomosc) arg;
            if (wiadomosc.getPoziomWiadomosci() > poziomPaniki) {
                System.out.println("Panika " + name + "! Wiadomosc: " + wiadomosc.getWiadomosc());
            } else {
                System.out.println("O, nowe wiadomosci o : " + wiadomosc.getWiadomosc());
            }
        }
    }
}
