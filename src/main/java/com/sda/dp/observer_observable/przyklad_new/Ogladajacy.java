package com.sda.dp.observer_observable.przyklad_new;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

import java.util.Observable;
import java.util.Observer;

public class Ogladajacy implements ChangeListener<Wiadomosc> {
    private int poziomPaniki;
    private String name;

    public Ogladajacy(int poziomPaniki, String name) {
        this.poziomPaniki = poziomPaniki;
        this.name = name;
    }

    @Override
    public void changed(ObservableValue<? extends Wiadomosc> observable, Wiadomosc oldValue, Wiadomosc newValue) {
        if (newValue.getPoziomWiadomosci() > poziomPaniki) {
            System.out.println("Panika " + name + "! Wiadomosc: " + newValue.getWiadomosc());
        } else {
            System.out.println("O, nowe wiadomosci o : " + newValue.getWiadomosc());
        }
    }
}
