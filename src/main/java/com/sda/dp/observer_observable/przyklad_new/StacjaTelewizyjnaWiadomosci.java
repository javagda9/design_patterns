package com.sda.dp.observer_observable.przyklad_new;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.Observable;

public class StacjaTelewizyjnaWiadomosci {

    private ObjectProperty<Wiadomosc> najnowszaWiadomość = new SimpleObjectProperty<>();

    public void dodajOgladajacego(Ogladajacy ogladajacy) {
        najnowszaWiadomość.addListener(ogladajacy);
    }

    public void powiadom(int poziom, String oWiadomosci) {
        Wiadomosc news = new Wiadomosc(poziom, oWiadomosci);

        najnowszaWiadomość.setValue(news);
    }
}
