package com.sda.dp.observer_observable.przyklad_new;

public class Wiadomosc {
    private int poziomWiadomosci;
    private String wiadomosc;

    public Wiadomosc(int poziomWiadomosci, String wiadomosc) {
        this.poziomWiadomosci = poziomWiadomosci;
        this.wiadomosc = wiadomosc;
    }

    public int getPoziomWiadomosci() {
        return poziomWiadomosci;
    }

    public void setPoziomWiadomosci(int poziomWiadomosci) {
        this.poziomWiadomosci = poziomWiadomosci;
    }

    public String getWiadomosc() {
        return wiadomosc;
    }

    public void setWiadomosc(String wiadomosc) {
        this.wiadomosc = wiadomosc;
    }
}
