package com.sda.dp.observer_observable.przyklad_new;

public class Main {
    public static void main(String[] args) {
        StacjaTelewizyjnaWiadomosci stacjaTelewizyjnaWiadomosci = new StacjaTelewizyjnaWiadomosci();
        stacjaTelewizyjnaWiadomosci.dodajOgladajacego(new Ogladajacy(5, "Marian"));
        stacjaTelewizyjnaWiadomosci.dodajOgladajacego(new Ogladajacy(3, "Arek"));
        stacjaTelewizyjnaWiadomosci.dodajOgladajacego(new Ogladajacy(6, "Darek"));
        stacjaTelewizyjnaWiadomosci.dodajOgladajacego(new Ogladajacy(7, "Marek"));
        stacjaTelewizyjnaWiadomosci.dodajOgladajacego(new Ogladajacy(8, "Franek"));
        stacjaTelewizyjnaWiadomosci.dodajOgladajacego(new Ogladajacy(9, "Julia"));

        stacjaTelewizyjnaWiadomosci.powiadom(7, "Trump prezydentem");

    }
}
