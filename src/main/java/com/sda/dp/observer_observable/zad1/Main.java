package com.sda.dp.observer_observable.zad1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        SmsStation station = new SmsStation();

        boolean isWorking = true;
        while (isWorking) {
            String linia = scanner.nextLine();
            if (linia.startsWith("addPhone")) {
                station.addPhone(new Phone(linia.substring(9)));
            } else {
                String[] kolumny = linia.split(" ", 2);
                station.sendSms(kolumny[0], kolumny[1]);
            }
        }
    }
}
