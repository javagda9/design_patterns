package com.sda.dp.observer_observable.zad1;

import java.util.Observable;

public class SmsStation extends Observable {
    public void addPhone(Phone p){
        addObserver(p);
    }

    public void sendSms(String number, String content){
        setChanged();
        notifyObservers(new Message(number, content));
    }
}
