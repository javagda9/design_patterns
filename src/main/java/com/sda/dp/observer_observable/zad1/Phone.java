package com.sda.dp.observer_observable.zad1;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class Phone implements Observer {
    private String number;
    private List<Message> messages;

    public Phone(String number) {
        this.number = number;
        this.messages = new ArrayList<>();
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Message) {
            Message wiad = (Message) arg;
            if (wiad.getNumber().equalsIgnoreCase(number)) {
                System.out.println("(" + number + ") Otrzymałeś wiadomość: " + wiad.getContent());
            } else {
                System.out.println("(" + number + ") Ignoruje");
            }
        }
    }
}
