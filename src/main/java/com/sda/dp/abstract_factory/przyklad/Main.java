package com.sda.dp.abstract_factory.przyklad;

import com.sda.dp.abstract_factory.przyklad.cars.CarFactory;
import com.sda.dp.abstract_factory.przyklad.cars.ICar;
//import com.sda.dp.abstract_factory.przyklad.cars.Car;

public class Main {
    public static void main(String[] args) {
        ICar car = CarFactory.createAudiA3Car();

    }
}
