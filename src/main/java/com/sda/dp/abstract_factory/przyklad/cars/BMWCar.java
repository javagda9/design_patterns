package com.sda.dp.abstract_factory.przyklad.cars;

public class BMWCar extends Car {
    protected BMWCar(String model, String producent, double engineCapacity, int yearMade, int horsepower, int mileage) {
        super(model, producent, engineCapacity, yearMade, horsepower, mileage);
    }

    public void metoda(){
        System.out.println(model.length());
    }
}
