package com.sda.dp.abstract_factory.przyklad.cars;

public abstract class CarFactory {
    public static Car createAudiA3Car(){
        return new Car("A3", "Audi", 1.6, 2000, 110, 0);
    }

    public static Car createBMWe46Car(){
        return new Car("e46", "BMW", 1.6, 2000, 110, 0);
    }

    public static Car createPolonez(){
        return new Car("Caro", "FSO", 1.0, 1995, 60, 0);
    }

    public static Car createFiat126p(){
        return new Car("126p", "Fiat", 0.6, 1992, 210, 0);
    }
}

//public class CarFactory {
//    private int licznikWyprodukowanychSamochodow = 0;
//    public Car createAudiA3Car(){
//        licznikWyprodukowanychSamochodow++;
//        return new Car("A3", "Audi", 1.6, 2000, 110, 0);
//    }
//
//    public Car createBMWe46Car(){
//        licznikWyprodukowanychSamochodow++;
//        return new Car("e46", "BMW", 1.6, 2000, 110, 0);
//    }
//
//    public Car createPolonez(){
//        licznikWyprodukowanychSamochodow++;
//        return new Car("Caro", "FSO", 1.0, 1995, 60, 0);
//    }
//
//    public Car createFiat126p(){
//        licznikWyprodukowanychSamochodow++;
//        return new Car("126p", "Fiat", 0.6, 1992, 210, 0);
//    }
//}