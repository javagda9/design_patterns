package com.sda.dp.abstract_factory.przyklad.cars;

public class Car implements ICar{
    String model;
    private String producent;
    private double engineCapacity;
    private int yearMade;
    private int horsepower;
    private int mileage;

    // metoda factory
    public static Car createBMWe36Car(){
        return new Car("e36", "BMW", 1.6, 2000, 110, 0);
    }

    protected Car(String model, String producent, double engineCapacity, int yearMade, int horsepower, int mileage) {
        this.model = model;
        this.producent = producent;
        this.engineCapacity = engineCapacity;
        this.yearMade = yearMade;
        this.horsepower = horsepower;
        this.mileage = mileage;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getProducent() {
        return producent;
    }

    public void setProducent(String producent) {
        this.producent = producent;
    }

    public double getEngineCapacity() {
        return engineCapacity;
    }

    public void setEngineCapacity(double engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    public int getYearMade() {
        return yearMade;
    }

    public void setYearMade(int yearMade) {
        this.yearMade = yearMade;
    }

    public int getHorsepower() {
        return horsepower;
    }

    public void setHorsepower(int horsepower) {
        this.horsepower = horsepower;
    }

    public int getMileage() {
        return mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }
}
