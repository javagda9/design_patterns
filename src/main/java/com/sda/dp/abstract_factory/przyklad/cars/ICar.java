package com.sda.dp.abstract_factory.przyklad.cars;

public interface ICar {
    public String getModel();
    public String getProducent();
}
