package com.sda.dp.strategy.przyklad.strategies;

public class SwordStrategy implements IAttackStrategy {
    @Override
    public void fight() {
        System.out.println("Walczę mieczem!");
    }
}
