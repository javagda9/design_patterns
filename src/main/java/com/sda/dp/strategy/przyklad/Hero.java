package com.sda.dp.strategy.przyklad;

import com.sda.dp.strategy.przyklad.strategies.IAttackStrategy;

public class Hero {
    private String name;
    private IAttackStrategy strategy;

    public Hero(String name) {
        this.name = name;
    }

    public void setStrategy(IAttackStrategy strategy) {
        this.strategy = strategy;
    }

    public void fightTheDragon(){
        strategy.fight();
    }
}
