package com.sda.dp.strategy.przyklad.strategies;

public interface IAttackStrategy {
    void fight();
}
