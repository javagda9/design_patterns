package com.sda.dp.strategy.przyklad;

import com.sda.dp.strategy.przyklad.strategies.BowStrategy;
import com.sda.dp.strategy.przyklad.strategies.SheepStrategy;
import com.sda.dp.strategy.przyklad.strategies.SwordStrategy;

import java.sql.Timestamp;

public class Main {
    public static void main(String[] args) {
        Hero bohater = new Hero("Marian");

        bohater.setStrategy(new BowStrategy());

        bohater.fightTheDragon();
    }
}
