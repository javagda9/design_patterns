package com.sda.dp.strategy.przyklad.strategies;

public class BowStrategy implements IAttackStrategy {
    @Override
    public void fight() {
        System.out.println("Walczę szczałami!");
    }
}
