package com.sda.dp.strategy.flight;

import java.util.List;

public interface FlightsParser {
    public int getNumberOfLinesRequired();

    public FlightInfo parseFlight(List<String> lines);
}
