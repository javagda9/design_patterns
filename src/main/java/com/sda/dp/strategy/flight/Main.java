package com.sda.dp.strategy.flight;

import com.sda.dp.strategy.flight.parsers.ParserLufthansa;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        Loader loader = new Loader();
        loader.setFilename("lufthansa.txt");
        loader.setParser(new ParserLufthansa());
        List<FlightInfo> infos = loader.parseFile();

        System.out.println(infos);
    }
}
