package com.sda.dp.strategy.flight;

public class ReservationInfo {
    private String surname;
    private int seatNumber;
    private String reservationId;

    public ReservationInfo(String surname, int seatNumber, String reservationId) {
        this.surname = surname;
        this.seatNumber = seatNumber;
        this.reservationId = reservationId;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(int seatNumber) {
        this.seatNumber = seatNumber;
    }

    public String getReservationId() {
        return reservationId;
    }

    public void setReservationId(String reservationId) {
        this.reservationId = reservationId;
    }

    @Override
    public String toString() {
        return "ReservationInfo{" +
                "surname='" + surname + '\'' +
                ", seatNumber=" + seatNumber +
                ", reservationId='" + reservationId + '\'' +
                '}';
    }
}

