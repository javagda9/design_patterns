package com.sda.dp.strategy.flight;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Loader {
    private String filename;
    private FlightsParser parser;

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setParser(FlightsParser parser) {
        this.parser = parser;
    }

    public List<FlightInfo> parseFile() {
        List<FlightInfo> infos = new ArrayList<>();
        try (Scanner reader = new Scanner(new FileReader(filename))) {
            String linia = "";
            while (reader.hasNextLine()) {
                List<String> lines = new ArrayList<>();
                for (int i = 0; i < parser.getNumberOfLinesRequired(); i++) {
                    if (!reader.hasNextLine()) {
                        break;
                    }
                    linia = reader.nextLine();
                    lines.add(linia);
                }
                infos.add(parser.parseFlight(lines));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return infos;
    }

}
