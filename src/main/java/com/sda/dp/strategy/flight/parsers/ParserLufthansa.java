package com.sda.dp.strategy.flight.parsers;

import com.sda.dp.strategy.flight.FlightInfo;
import com.sda.dp.strategy.flight.FlightsParser;
import com.sda.dp.strategy.flight.ReservationInfo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class ParserLufthansa implements FlightsParser {
    private static final String SEPARATOR_SECTIONS = ";";
    private static final String SEPARATOR_ELEMENTS = "%";
    private static final String SEPARATOR_PARAMETERS = "#";

    private static final int POSITION_FLIGHT_ID = 0;
    private static final int POSITION_TIME_DEP = 1;
    private static final int POSITION_TIME_ARRI = 2;
    private static final int POSITION_SEATS_REGULAR = 3;
    private static final int POSITION_SEATS_BUSINESS = 4;
    private static final int POSITION_RESERVATIONS = 5;

    @Override
    public int getNumberOfLinesRequired() {
        return 1;
    }

    @Override
    public FlightInfo parseFlight(List<String> lines) {
        String line = lines.get(0);

        String[] sections = line.split(SEPARATOR_SECTIONS);

        FlightInfo flightInfo = new FlightInfo(
                sections[POSITION_FLIGHT_ID],
                sections[POSITION_TIME_DEP],
                sections[POSITION_TIME_ARRI],
                splitIntoList(sections[POSITION_SEATS_REGULAR]),
                splitIntoList(sections[POSITION_SEATS_BUSINESS]));

        if (sections.length > POSITION_RESERVATIONS) {
            flightInfo.setReservations(parseReservations(sections[POSITION_RESERVATIONS]));
        }

        return flightInfo;
    }

    private List<ReservationInfo> parseReservations(String section) {
        String[] elements = section.split(SEPARATOR_ELEMENTS);

        List<ReservationInfo> reservationInfos = new ArrayList<>();
        for (String element : elements) {
            String[] parameters = element.split(SEPARATOR_PARAMETERS);
            ReservationInfo reservationInfo = new ReservationInfo(
                    parameters[0],
                    Integer.parseInt(parameters[1]),
                    parameters[2]);
            reservationInfos.add(reservationInfo);
        }
        return reservationInfos;
    }

    private List<Integer> splitIntoList(String section) {
        List<Integer> integers = new ArrayList<>();
        String[] elements = section.split(SEPARATOR_ELEMENTS);

        integers.addAll(Arrays.stream(elements)
                .map(el -> Integer.parseInt(el))
                .collect(Collectors.toList()));
        return integers;
    }
}
