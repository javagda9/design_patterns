package com.sda.dp.strategy.flight;

import java.util.ArrayList;
import java.util.List;

public class FlightInfo {
    private String flightNumber;
    private String timestampDeparture;
    private String timestampArrival;
    private List<Integer> seatsRegular;
    private List<Integer> seatsBusiness;
    private List<ReservationInfo> reservations;

    public FlightInfo(String flightNumber, String timestampDeparture, String timestampArrival, List<Integer> seatsRegular, List<Integer> seatsBusiness) {
        this.flightNumber = flightNumber;
        this.timestampDeparture = timestampDeparture;
        this.timestampArrival = timestampArrival;
        this.seatsRegular = seatsRegular;
        this.seatsBusiness = seatsBusiness;
        this.reservations = new ArrayList<>();
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getTimestampDeparture() {
        return timestampDeparture;
    }

    public void setTimestampDeparture(String timestampDeparture) {
        this.timestampDeparture = timestampDeparture;
    }

    public String getTimestampArrival() {
        return timestampArrival;
    }

    public void setTimestampArrival(String timestampArrival) {
        this.timestampArrival = timestampArrival;
    }

    public List<Integer> getSeatsRegular() {
        return seatsRegular;
    }

    public void setSeatsRegular(List<Integer> seatsRegular) {
        this.seatsRegular = seatsRegular;
    }

    public List<Integer> getSeatsBusiness() {
        return seatsBusiness;
    }

    public void setSeatsBusiness(List<Integer> seatsBusiness) {
        this.seatsBusiness = seatsBusiness;
    }

    public List<ReservationInfo> getReservations() {
        return reservations;
    }

    public void setReservations(List<ReservationInfo> reservations) {
        this.reservations = reservations;
    }

    @Override
    public String toString() {
        return "FlightInfo{" +
                "flightNumber='" + flightNumber + '\'' +
                ", timestampDeparture='" + timestampDeparture + '\'' +
                ", timestampArrival='" + timestampArrival + '\'' +
                ", seatsRegular=" + seatsRegular +
                ", seatsBusiness=" + seatsBusiness +
                ", reservations=" + reservations +
                '}';
    }
}
